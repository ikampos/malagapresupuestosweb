# -*- coding: utf-8 -*-
from django import forms


class ContactForm(forms.Form):
    message = forms.CharField(required=False, widget=forms.Textarea(
        attrs={
            'placeholder':
            'Escribe tu mensaje, puedes pedir presupuesto sin compromiso'}),
        max_length=1000)
    email = forms.EmailField(required=False, max_length=100,
                             widget=forms.EmailInput(attrs={
                                                     'placeholder': 'Tu email'
                                                     }))
    phone = forms.CharField(required=False, max_length=12, min_length=9,
                            widget=forms.TextInput(
                                attrs={'placeholder': 'Tu número de teléfono'
                                       }))
    # phone = forms.IntegerField(required = False, \
    # widget=forms.NumberInput(attrs={
    # 'placeholder': 'Tu número de teléfono'}))

    def clean(self):
        if not (self.cleaned_data.get('message') or
                self.cleaned_data.get('email')
                or self.cleaned_data.get('phone')):
            raise forms.ValidationError(
                "¡¡ Por favor, indique al menos uno de los datos de contacto, \
                email o teléfono !!'")
        return self.cleaned_data
