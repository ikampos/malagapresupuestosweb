# urls.py
from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
                       url(r'legal-advice/$',
                           legal_advice, name='legal_advice'),
                       url(r'privacy-policy/$',
                           privacy_policy, name='privacy_policy'),
                       url(r'cookies-policy/$',
                           cookies_policy, name='cookies_policy'),
                       url(r'$', home, name='home'),
                       )
