# -*- coding: utf-8 -*-
from django.shortcuts import render
from .forms import ContactForm
from django.core.mail import send_mail


def home(request):

    context = {}
    form = ContactForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            subject = 'Contact from Malagapresupuestosweb.com'
            message = form.cleaned_data['message'] or 'Sin mensaje...'
            sender = form.cleaned_data[
                'email'] or 'noemail@malagapresupuestos.com'
            phone = form.cleaned_data['phone']
            message = message + '\nTlf: ' + str(phone) + '\nEmail: ' + sender

            recipients = ['info@malagapresupuestosweb.com']

            send_mail(subject, message, sender, recipients)
            context[
                'message'] = '¡¡ Muchas gracias por contactar con \
                Malagapresupuestosweb.com !!'

    context['form'] = form

    # import pdb; pdb.set_trace()
    return render(request, 'home.html', context)


def legal_advice(request):
    return render(request, 'legal_advice.html', {})


def privacy_policy(request):
    return render(request, 'privacy_policy.html', {})


def cookies_policy(request):
    return render(request, 'cookies_policy.html', {})
